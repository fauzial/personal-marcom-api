Number.prototype.zero = function (digit) {
    var numSeri = String(this)
    while(numSeri.length < (digit || 2)){
        numSeri = '0' + numSeri
    }
    return numSeri
}

const idHelper = {
    company: (comData) => {
        if(comData.length>0){
            let lastCode = comData[0].code
            let lastNum = parseInt(lastCode.replace('CP', ''))

            lastNum += 1
            lastCode = (lastNum).zero(4)
            return ('CP' + lastCode) 
        }else{
            return 'CP0001'
        }
    }
}

module.exports = idHelper