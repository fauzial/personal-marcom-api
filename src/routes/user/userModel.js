function mUser(userData){
    //THIS IS THE RIGH MODEL
    /* this._id        = userData._id
    this.username   = userData.username
    this.password   = userData.password
    this.m_role_id    = userData.m_role_id
    this.m_employee_id= userData.m_employee_id
    this.is_delete   = userData.is_delete || false
    this.created_by  = userData.created_by
    this.created_date= userData.created_date
    this.updated_by  = userData.updated_by
    this.updated_date= userData.updated_date */

    //THIS IS THE SAMPLE MODEL FOR CREATING DUMMY USER
    //to TESTING JWT
    this._id        = userData._id
    this.username   = userData.username
    this.password   = userData.password
    this.m_role_id    = '0'
    this.m_employee_id= '0'
    this.is_delete   = userData.is_delete || false
    this.created_by  = 'God itself!'
    this.created_date= userData.created_date
    this.updated_by  = userData.updated_by
    this.updated_date= userData.updated_date
}

module.exports = mUser