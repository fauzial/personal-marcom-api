const dbName = 'm_user'

const userView = {
    getAll: (cb) => {
        db.collection(dbName).find({is_delete : false}).toArray().then(docs => {
            if(docs){
                cb(200, docs)
            }else{
                cb(507, 'Trouble with database.')
            }
        })    
    },
    getById: (id, cb) => {
        db.collection(dbName).findOne({ _id: id }).then(doc => {
            if(doc){
                cb(200, doc)
            }else{
                cb(507, 'Trouble with database.')
            }
        })
    },
    getByUsername: (username, cb) => {
        db.collection(dbName).findOne({ username: username }).then(doc => {
            if(doc){
                cb(400, 'Username already exist.')
            }else{
                cb(200)
            }
        })
    },
    postOne: (data, cb) => {
        db.collection(dbName).insertOne(data)
        .then(doc => {
            cb(201, 'A new user account has been added.')
        }).catch(err => {
            cb(507, 'Fail to insert new data.')
            console.log(err)
        })
    },
    putOne: (id, data, cb) => {
        db.collection(dbName).updateOne({_id: id}, data)
        .then(doc => {
            cb(201, 'User data has been edited.')
        }).catch(err => {
            cb(507, 'Fail to edit the data.')
            console.log(err)
        })
    },
    deleteOne: (id, cb) => {
        db.collection(dbName).updateOne({_id: id},{
            $set: { is_delete: true }
        }).then(doc => {
            cb(201, 'User data has been deleted.')
        }).catch(err => {
            cb(507, 'Fail to delete the data.')
            console.log(err)
        })
    }
}

module.exports = userView